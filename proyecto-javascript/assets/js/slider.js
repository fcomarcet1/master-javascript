"use strict";
$(function () {
    //* Slider
    // con este if solo usamos esta js en la pagina index
    if (window.location.href.indexOf('index')){
        var slider = $(".bxslider");
        slider.bxSlider({
            auto: true,
            autoControls: true,
            stopAutoOnClick: true,
            pager: true,
            slideWidth: 1200,
        });
    }

});
