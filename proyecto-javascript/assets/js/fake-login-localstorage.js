'use strict';
$(function(){

    //* COMPROBAR DISPONIBILIDAD
    if (typeof Storage !== "undefined") {
        console.log("LocalStorage compatible");
    } else {
        console.log("LocalStorage Incompatible");
    }

    const form = $("#login_form");
    form.on('submit', function () {

        var name = $('#name').val();
        //*ALMACENAR DATOS (strings, numeros), setItem("key", value)
        localStorage.setItem("name", name);

    });

    //* RECUPERAR ELEMENTO Y MOSTRARLO EN LA WEB
    let storage_name = localStorage.getItem("name");
    //console.log(storage_name);

    if (storage_name != null && storage_name != 'undefined'){

        //Seleccionamos el div con id="about", y añadimos los datos del storage almacenados en la variable storage_titulo
        let about_parrafo = $('#about p');
        about_parrafo.html(`Bienvenido: ${storage_name}`);
        about_parrafo.append("<br>")
        about_parrafo.append("<a href='#' id='logout'>logout</a> ");

        // Ocultamos div
        $('#login').hide();

        // limpiamos local storage y actualizamos la pagina.
        $('#logout').on('click', function () {
            localStorage.clear();
            location.reload();
        });

    }



});