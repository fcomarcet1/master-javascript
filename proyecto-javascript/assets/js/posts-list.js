"use strict";
$(function () {
    // mostrar solo en el index
    if (window.location.href.indexOf('index')){
        //* Listado dinamico de Posts desde json
        let posts = [
            {
                title: "Titulo 1",
                date: `Publicado: ${moment().format("LLL")}`,
                content: `Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? 
                    Blanditiis itaque commodi alias officia tempore adipisci.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.
            `,
            },
            {
                title: "Titulo 2",
                date: `Publicado: ${moment().format("LLL")}`,
                content: `Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? 
                    Blanditiis itaque commodi alias officia tempore adipisci.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.
            `,
            },
            {
                title: "Titulo 3",
                date: `Publicado: ${moment().format("LLL")}`,
                content: `Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Et fuga id esse, assumenda vero fugiat
                    facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia
                    tempore adipisci.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat
                    facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia
                    tempore adipisci.
                    
                  `,
            },
            {
                title: "Titulo 4",
                date: `Publicado: ${moment().format("LLL")}`,
                content: `Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? 
                    Blanditiis itaque commodi alias officia tempore adipisci.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.
            `,
            },
            {
                title: "Titulo 5",
                date: `Publicado: ${moment().format("LLL")}`,
                content: `Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? 
                    Blanditiis itaque commodi alias officia tempore adipisci.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et fuga id esse, assumenda vero fugiat facere perferendis, cum porro recusandae nesciunt qui harum? Blanditiis itaque commodi alias officia tempore adipisci.
            `,
            },
        ];

        //console.log(posts);
        //* Recorrer json
        posts.forEach((element, index) => {
            // plantilla
            var post = `
                <article class="post">
                    <h2>${element.title}</h2>
                    <span class="date">${element.date}</span>
                    <p>
                        ${element.content} 
                    </p>
                    <a href="#" class="button-more">Leer más</a>
                </article>
                `;
            //console.log(post);
            //* Insertar en el div
            var div_post = $("#posts");
            div_post.append(post);
        });
    }

});
