"use strict";
$(function () {
    let style_sheet = $("#theme");
    let div_theme_red = $("#to-red");
    let div_theme_green = $("#to-green");
    let div_theme_blue = $("#to-blue");

    div_theme_red.on("click", function () {
        style_sheet.attr("href", "assets/css/theme-red.css");
    });

    div_theme_green.on("click", function () {
        style_sheet.attr("href", "assets/css/theme-green.css");
    });

    div_theme_blue.on("click", function () {
        style_sheet.attr("href", "assets/css/theme-blue.css");
    });
});
