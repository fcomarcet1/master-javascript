'use strict';
$(function (){
    $('.scroll-up').on('click', function (e){
        // para que el link no nos rediri a ningun sitio y pueda ejecutarse
        // la funcionalidad.
        e.preventDefault()

        $("HTML, BODY").animate({
            scrollTop: 0
        }, 1000);
    });

    return false;

    // Mejor ejemplo que el mio.
    // https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_scroll_to_top
})