"use strict";
$(function () {
    console.log("cargado tooltips UI");

    //* TOOLTIPS
    $(document).tooltip();

    //* CUADROS DE DIALOGO
    var boton = $("#boton");

    boton.on("click", function () {
        var div_cuadro_dialogo = $("#popup");
        div_cuadro_dialogo.dialog();
    });

    //* DATEPICKER
    var calendar = $("#calendar");
    calendar.datepicker();

    console.log(calendar.datepicker().val);

    //* PESTAÑAS(tabs)
    $("#tabs").tabs();
});
