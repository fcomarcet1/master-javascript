$(function () {
    console.log("cargado fichero interacciones jQuery");

    //* DRAGGABLE
    //*Move the draggable object by clicking on it with the mouse and dragging it anywhere within the viewport.
    var caja = $(".caja");
    caja.draggable();

    //* RESIZABLE
    //* Cargamos css de jquery ui despues de jquery
    caja.resizable();

    //* SELECTABLE
    //* Permite selecionar elementos de una lista
    var lista = $(".lista-seleccionable");
    //lista.selectable();

    //* SORTABLE
    //* Nos permite ordenar elementos de una lista
    lista.sortable({
        update: function (event, ui) {
            console.log("Ha cambiado la lista");
        },
    });

    //* DROPPABLE
    var area = $("#area");
    var div_mov = $("#elemento-mover");

    div_mov.draggable();
    area.droppable({
        drop: function () {
            console.log("Has soltado el elemento");
        },
    });
});
