"use strict";
/*
    Crear un formulario para añadir peliculas y mostrar los elementos añadidos

*/
/* window.addEventListener("load", () => {}); */

/* //* Seleccionamos el form
var form = document.querySelector("#form-peliculas");
form.addEventListener("submit", () => {
	//obtener titulo de input
	var titulo = document.querySelector("#nombre-pelicula").value;

	//Comprobar que el campo no llega vacio
	if (titulo.length >= 1) {
		console.log(titulo.length);
		//Almacenar en el storage
		localStorage.setItem(titulo, titulo);
		//localStorage.setItem("titulo", titulo); con "titulo se machaca el indice
	}
});

//* Mostrar por pantalla
var div_peliculas = document.querySelector(".peliculas");
for (const index in localStorage) {
	// evaluamos que solo sea string para no ver metodos asociados a localStorage
	if (typeof localStorage[index] === "string") {
		//Creamos elementos <li></li>
		var li = document.createElement("li");
		li.append(localStorage[index]);
		div_peliculas.append(li);
	}
}

//* Form eliminar pelicula
var formDelete = document.querySelector("#form-peliculas-delete");
formDelete.addEventListener("submit", () => {
	console.log(formDelete);
	//obtener titulo de input
	var tituloDelete = document.querySelector("#nombre-pelicula-delete").value;
	//Comprobar que el campo no llega vacio
	if (tituloDelete.length >= 1) {
		//Eliminar en el storage
		localStorage.removeItem(tituloDelete);
	}
}); */

"use strict";

//* Form Añadir pelicula
var formulario = document.querySelector("#formpeliculas");
formulario.addEventListener("submit", function () {
	var titulo = document.querySelector("#addpelicula").value;
	//Comprobamos que no llega vacio el campo
	if (titulo.length >= 1) {
		localStorage.setItem(titulo, titulo);
	}
});

//* Mostrar por pantalla en una lista
var ul = document.querySelector("#peliculas-list");
for (var i in localStorage) {
	// cogemos solo los strings para no imprimir metodos del locaStorage
	if (typeof localStorage[i] == "string") {
		var li = document.createElement("li");
		li.append(localStorage[i]);
		ul.append(li);
	}
}

//* Form Borrar pelicula
var formularioDelete = document.querySelector("#formBorrarPeliculas");
formularioDelete.addEventListener("submit", function () {
	var titulo = document.querySelector("#borrarPelicula").value;

	if (titulo.length >= 1) {
		localStorage.removeItem(titulo);
	}
});
