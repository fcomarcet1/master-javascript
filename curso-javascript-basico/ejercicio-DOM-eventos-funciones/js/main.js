"use strict";

window.addEventListener("load", () => {
	console.log("DOM cargado");

	//* Capturar formulario
	var formulario = document.querySelector("#formulario");

	//Ocultar caja para mostrar datos solo se vera cuanso demos submit
	var box_dashed = document.querySelector("#dashed");
	box_dashed.style.display = "none";

	formulario.addEventListener("submit", () => {
		console.log("Evento submit capturado");

		//* Obtener valores de los campos del formulario
		var nombre = document.querySelector("#nombre").value;
		var apellidos = document.querySelector("#apellidos").value;
		var edad = document.querySelector("#edad").value;

		// mostramos caja que estaba oculta
		box_dashed.style.display = "block";

		//* 1º METODO
		// Añadimos los parrafos en el codigo html , mediante el query selector
		var p_nombre = document.querySelector("#p_nombre span");
		var p_apellidos = document.querySelector("#p_apellidos span");
		var p_edad = document.querySelector("#p_edad span");

		p_nombre.innerHTML = nombre;
		p_apellidos.innerHTML = apellidos;
		p_edad.innerHTML = edad;

		//* OTRO METODO
		// añadimos los datos a un array y lo recorremos
		/* 	var datos_form = [nombre, apellidos, edad];

		for (const indice in datos_form) {
			if (datos_form.hasOwnProperty(indice)) {
				//crear elementos <p></p> html para mostrar la info
				var parrafo = document.createElement("p");
				// añadimos datos a cada parrafo
				parrafo.append(datos_form[indice]);

				// añadimos los parrafos a la caja dashed
				box_dashed.append(parrafo);
				const element = datos_form[indice];
			}
		} */
	}); //End submit
}); // End load
