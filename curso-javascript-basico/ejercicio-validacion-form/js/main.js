//* *************************** VALIDACION FORMULARIO******************

"use strict";

window.addEventListener("load", () => {
	//Seleccionar form
	var formulario = document.querySelector("#formulario");
	//Capturar evento submit
	formulario.addEventListener("submit", () => {
		//Obtenemos valores del form
		var nombre = document.querySelector("#nombre").value;
		var apellidos = document.querySelector("#apellidos").value;
		var edad = parseInt(document.querySelector("#edad").value);

		console.log(typeof edad, edad);

		//*Nombre
		//Campo vacio
		if (nombre.trim() == null || nombre.trim().length == 0) {
			//alert("Campo nombre vacio");
			document.querySelector(".error-nombre").innerHTML = "El campo nombre no puede estar vacio";
			return false;
		} else {
			document.querySelector(".error-nombre").style.display = "none";
			console.log("campo nombre OK");
		}
		// Campo no numerico
		if (!isNaN(nombre)) {
			//alert("el campo nombre es un nº");
			document.querySelector(".error-nombre").innerHTML = "El nombre no es valido";
			return false;
		} else {
			document.querySelector(".error-nombre").style.display = "none";
			console.log("campo nombre no es un nº");
		}

		//*Apellidos
		//Campo vacio
		if (apellidos.trim() == null || apellidos.trim().length == 0 || /^\s+$/.test(apellidos)) {
			//alert("Campo apellidos vacio");
			document.querySelector(".error-apellidos").innerHTML = "El campo apellidos no puede estar vacio";

			return false;
		} else {
			document.querySelector(".error-apellidos").style.display = "none";
			console.log("campo apellidos OK");
		}
		// Campo no numerico
		if (!isNaN(apellidos)) {
			//alert("el campo apellidos es un nº");
			document.querySelector(".error-apellidos").innerHTML = "Los apellidos no son validos";
			return false;
		} else {
			document.querySelector(".error-apellidos").style.display = "none";
			console.log("campo apellidos no es un nº");
		}

		//*Edad
		// campo vacio
		// si no metemos nada  devuelve NaN, si introducimos el nº devuelve el numero
		// el método "isNaN" comprueba si el valor No es un número
		if (edad == null || edad <= 0 || isNaN(edad)) {
			//alert("Campo edad vacio");
			document.querySelector(".error-edad").innerHTML = "El campo edad no puede estar vacio";
			return false;
		} else {
			document.querySelector(".error-edad").style.display = "none";
			console.log("campo edad OK");
		}

		console.log("Enviamos form");
	});
});
