/** @format */

"use strict";

//* **********     Curso JavaScript: 10. Funciones      ********** */

/*
        Una función es un bloque de código, autocontenido, que se puede definir una vez y ejecutar en cualquier momento. Opcionalmente, una función puede aceptar parámetros y devolver un valor.
        Las funciones en JavaScript son objetos, un tipo especial de objetos:
        Se dice que las funciones son ciudadanos de primera clase porque pueden asignarse a un valor, y pueden pasarse como argumentos y usarse como un valor de retorno.
*/

//Declaración de función
function estoEsUnaFuncion() {
	console.log("Uno");
	console.log("Dos");
	console.log("Tres");
}

//Invocación de función
estoEsUnaFuncion();
estoEsUnaFuncion();
estoEsUnaFuncion();
estoEsUnaFuncion();

function unaFuncionQueDevuelveValor() {
	console.log("Uno");
	return 19;

	/* todo este codigo ya no se ejecutara despues del return */
	console.log("Dos");
	console.log("Tres");
	return "La función ha retornado una Cadena de texto";
}

let valorDeFuncion = unaFuncionQueDevuelveValor();
console.log(valorDeFuncion);

// Funcion con parametros
function calculadora(num1, num2) {
	let result = num1 + num2;
	return result;
}

//funcion con parametros opcionales
function saludar(nombre = "Desconocido", edad = 0) {
	console.log(`Hola mi nombre es ${nombre} y tengo ${edad} años.`);
}

saludar("kEnAi", 7);
saludar();

function calculadora2(numero1, numero2, mostrar = false) {
	if (mostrar) {
		console.log("Hemos introducido los 3 parametros de la funcion ");
		let resultado = numero1 + numero2;
		return resultado;
	} else {
		console.log("Hemos introducido los 2 parametros obligatorios y no el opcional ");
		let resultado = numero1 * numero2;
		return resultado;
	}
}

calculadora2(25, 10, true); //Hemos introducido los 3 parametros de la funcion -> 35
calculadora2(5, 10); //Hemos introducido los 2 parametros obligatorios y no el opcional -> 50

// funciones dentro de oytas funciones
function suma(n1, n2) {
	let suma = n1 + n2;
	return suma;
}

function multiplicacion(n1, n2) {
	let suma = n1 + n2;
	return suma;
}

function calculadora3(numero1, numero2, operacion = false) {
	if (operacion) {
		// llamada a funcion dentro de otra funcion
		let result_suma = suma(numero1, numero2);
		return result_suma;
	} else {
		let result_mult = multiplicacion(numero1, numero2);
		return result_mult;
	}
}


//*FUNCIONES DECLARADAS VS FUNCIONES EXPRESADAS

funcionDeclarada();

function funcionDeclarada() {
	console.log(
		"Esto es un función declarada, puede invocarse en cualquier parte de nuestro código, incluso antes de que la función sea declarada"
	);
}

funcionDeclarada();

//funcionExpresada();

//*Función anónima
const funcionExpresada = function () {
	console.log(
		"Esto es una función expresada, es decir, una función que se le ha asignado como valor a una variable, si invocamos esta función antes de su definición JS nos dirá 'Cannot access 'funcionExpresada' before initialization'"
	);
};

funcionExpresada();
