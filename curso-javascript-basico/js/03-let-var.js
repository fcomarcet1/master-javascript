"use strict";

//* VAR vs LET

//* var -> AMBITO GLOBAL, se almacena en el objeto window
var numero = 50;
console.log(numero); //50

if (true) {
  var numero = 100;
  console.log(numero); //100
}

console.log(numero); //50

//*-------------------------------------------------------------------

//* let -> AMBITO DE BLOQUE
var curso = "master-en-javascript";
console.log(curso); //master-javascript

if (true) {
  let curso = "El mejor master de javascript";
  console.log(curso); // El mejor master de javascript
}

console.log(curso); //master-javascript

//*-------------------------------------------------------------------

console.log("****************var*****************");

var musica = "Rock";
console.log("Variable Música antes del Bloque", musica); // Rock
{
  var musica = "Pop";
  console.log("Variable Música dentro del Bloque", musica); //Pop
}
console.log("Variable Música después del Bloque", musica); // Pop

console.log("****************let*****************");

let musica2 = "Rock";
console.log("Variable Música antes del Bloque", musica2); //Rock
{
  let musica2 = "Pop";
  console.log("Variable Música dentro del Bloque", musica2); //Pop
}
console.log("Variable Música después del Bloque", musica2); // Rock
Rock;
