"use strict";

//* ********************* METODOS PARA STRINGS *************************

let numero = 666;
let texto1 = "Bienvenidos al curso de javascript";
let texto2 = " es muy buen curso";

// Pasar nº a string
console.log(numero.toString(), typeof numero.toString());

// Pasar a mayusculas y minusculas
console.log(texto1.toUpperCase(), texto1.toLowerCase());

//longitud de un string
console.log(texto1.length, texto2.length);

const array1 = [1, 2, 3, 4];
console.log(array1.length);

console.log("*********************************************************");

//Concatenar
let textoConcatenado = texto1 + texto2;
console.log(textoConcatenado);

let textoConcatenado2 = texto1.concat(" @" + texto2);
console.log(textoConcatenado2);
