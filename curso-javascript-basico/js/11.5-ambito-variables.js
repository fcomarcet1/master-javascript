"use strict";

function holaMundo(texto) {
	console.log(texto);
	console.log(numero);
}

//* estamos declarando la variable despues de definir una
//* funcion que la usa, y esto funciona en js
var texto = "Hola soy una variable global";
var numero = 55;

holaMundo(texto);

console.log("*******************************************************");

//*¿Y si definimos una variable dentro de la funcion?
function holaMundo2(texto) {
	var hola_mundo = "soy una variable dentro de la funcion";
	console.log(texto);
	console.log(numero.toString);
	console.log(hola_mundo);
}

holaMundo2(texto);

console.log(hola_mundo); //Error. hola_mundo is not defined

//* RESUMEN
//* Si declaramos las variables fuera de funcion seran de ambito global,
//* mientras que si las declaramos dentro de la funcion seran de ambito local a la funcion
