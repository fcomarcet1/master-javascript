//* **************** PETICIONES AJAX  ********************************
//* Vamos a meter el codigo de recorrer los datos en una funcion.
//* Imaginemos que queremos realizar una 2º peticion para obtener un usuario en concreto,
//* a continuacion de mostrar el listado de usuarios

"use strict";
var div_UsersList = document.querySelector("#users-list");
var div_User = document.querySelector("#user");
var div_Info = document.querySelector("#info");
var loading = document.querySelector(".loading");
var usersList = [];

//listado users
function GetUsers(usuarios) {
	return fetch("https://reqres.in/api/users?page=2");
}

// user
function GetUser(user) {
	return fetch("https://reqres.in/api/users/2");
}

function GetInfo() {
	var profesor = {
		nombre: "Fco marcet",
		Asignatura: "Javascript",
		curso: 2020,
	};
	return new Promise((resolve, reject) => {
		//Convertir a JsonString
		var profesor_string = "";
		// a proposito hacemos que tarde 3s
		setTimeout(() => {
			var profesor_string = JSON.stringify(profesor);
			if (typeof profesor_string != "string" || profesor_string == "") {
				//error
				return reject("error");
			} else {
				//OK
				return resolve(profesor_string);
			}
		}, 3000);
	});
}

// funcion recorrer listado usuarios
function ShowUsersList(users) {
	usersList.map((user, index) => {
		// creamos elemento h5 y añadimos los registros
		let h4 = document.createElement("h5");
		h4.innerHTML = `${user.first_name} ${user.last_name}`;
		// Añadimos al div los registros
		div_UsersList.appendChild(h4);
		// eliminamos
		loading.style.display = "none";
	});
}

// funcion mostar info de usuario
function ShowUser(user) {
	let h5 = document.createElement("h5");
	let avatar = document.createElement("img");

	// datos usuario
	h5.innerHTML = `${user.first_name} ${user.first_name}`;
	div_User.appendChild(h5);
	// avatar
	avatar.src = `${user.avatar}`;
	avatar.width = "50";
	div_User.appendChild(avatar);

	document.querySelector("#user .loading").style.display = "none";
}

function ShowInfo(data) {}

// Peticion AJAX-> fetch()
GetUsers()
	// cast a JsonString
	.then((data) => data.json())
	.then((users) => {
		// pasamos los datos al array
		usersList = users.data;
		// recorremos array
		ShowUsersList(usersList);
		// devolvemos la siguiente peticion fetch
		return GetInfo();
	})
	//* PROFESOR -> GetInfo() Ej nueva promesa
	.then((data_info) => {
		//console.log(data_info);
		div_Info.innerHTML = data_info;
		return GetUser();
	})
	//* USUARIO
	// cast a JsonString
	.then((data) => data.json())
	.then((user) => {
		//llamamos funcion que imprime user
		ShowUser(user.data);
	})
	//*  CAPTURAR ERRORES
	.catch((error) => {
		console.log(error);
	});
