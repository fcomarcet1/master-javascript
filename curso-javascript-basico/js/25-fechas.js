//* ******************** FECHAS ***********************
"use strict";

let fecha = new Date();
console.log(fecha); // Tue Nov 10 2020 11:13:51 GMT+0100 (hora estándar de Europa central)

let year = fecha.getFullYear();
console.log(year); // 2020

let mes = fecha.getMonth();
console.log(mes); //10 empieza a contar desde 0 Enero = 0

let dia = fecha.getDate();
console.log(dia); //10

let hora = fecha.getHours();
console.log(hora);
