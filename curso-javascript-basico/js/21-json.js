//* ******************** JSON (JavaScript Object Notacion) *****************
//* formato de texto sencillo para el intercambio de datos -> Objetos
//* no existen arrays asociativos como en PHP, sino que existen los JSON

"use stric";

const object = {
	a: 1,
	b: 2,
	c: 3,
};

//Objeto pelicula
const pelicula = {
	titulo: "ESDLA",
	year: 2017,
	pais: "EEUU",
	actor: "Falete",
};
console.log(pelicula);

//* Acceder a propiedades del objeto
let titulo_pelicula = pelicula.titulo;
let actor_pelicula = pelicula.actor;

console.log(titulo_pelicula, actor_pelicula);

//* modificar propiedades del objeto
//cambiar pais
let nuevoPais = "España";
pelicula.pais = nuevoPais;
console.log(pelicula);

//*Recorrer objeto
//for in (No usarlo para arrays)
const objeto = {
	a: 1,
	b: 2,
	c: 3,
};

for (const propiedad in objeto) {
	console.log(`${propiedad}: ${objeto[propiedad]}`);
}

const pelicula2 = {
	titulo: "Rambo",
	year: 1980,
	pais: "EEUU",
	actor: "Stallone",
};

console.log("**********************************************");
// tenemos un array con un objeto dentro y ademas vamos a añadir el objeto pelicula2
const peliculas = [{ titulo: "Spiderman", year: 2000, pais: "INDIA", actor: "XXX" }, pelicula2];

//*recorrer array con objetos y mostrarlos por pantalla
//Seleccionamos el div con class container
var container = document.querySelector(".container");

for (const index in peliculas) {
	//creamos el parrafo
	var p = document.createElement("p");
	//añadimos el parrafo con el titulo a peliculas en cada ciclo
	p.append(peliculas[index].titulo + " - " + peliculas[index].year);
	// añadimos al container los parrafos
	container.append(p);
}
