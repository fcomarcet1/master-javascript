//* Funciones para reemplazar, separar textos, cortar....

"use strict";

let texto1 = "Bienvenidos al curso de javascript ";
let texto2 = "es muy buen curso";

//* reemplazar un string por otro
let reemplazar = texto1.replace("javascript", "JS"); // Bienvenidos al curso de JS
console.log(reemplazar);

//*separar un string a partir de el caracter indicado
let reemplazar2 = texto1.slice(15); //curso de javascript
let reemplazar3 = texto1.slice(15, 30); //curso de javasc
console.log(reemplazar2);
console.log(reemplazar3);

//*recoger un string en un array
let reemplazar4 = texto2.split(); //[" es muy buen curso"]
let reemplazar5 = texto2.split(""); //[" ", "e", "s", " ", "m", "u", "y", " ", "b", "u", "e", "n", " ", "c", "u", "r", "s", "o"]
let reemplazar6 = texto2.split(" "); //["es", "muy", "buen", "curso"]
console.log(reemplazar6);

let texto3 = "Hola que, tal Pepe, Lotas";
let reemplazar7 = texto3.split(","); //["Hola que", " tal Pepe", " Lotas"]
console.log(reemplazar7);

//*eliminar espacios al inicio y al final
let cadena = "      Hola que tal, acabo de llegar a clase   ";
let resultado = cadena.trim();
console.log(cadena);
console.log(resultado);
