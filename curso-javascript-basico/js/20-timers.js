//* ***************************** TIMERS ******************************
"use strict";

window.addEventListener("load", () => {
	//* setTimeout() Ejecuta una accion una unica vez un vez trancurra el intervalo de tiempo.
	var timerOut = setTimeout(() => {
		console.log("Ejecutando setTimeout a los 5s");
		var titulo = document.querySelector("#encabezado0");
		var bgColor = titulo.style.background;

		if (bgColor == "cyan") {
			titulo.style.background = "white";
			titulo.style.color = "black";
		} else {
			titulo.style.background = "cyan";
			titulo.style.color = "pink";
		}
	}, 5000);

	//* setInterval() Ejecuta una accion cada cierto intervalo de tiempo
	var timerInterval = setInterval(() => {
		console.log("Ejecutando setInterval cada 1s");
		var titulo = document.querySelector("#encabezado");
		var bgColor = titulo.style.background;

		if (bgColor == "blue") {
			titulo.style.background = "white";
			titulo.style.color = "black";
		} else {
			titulo.style.background = "blue";
			titulo.style.color = "yellow";
		}

		//alert("Ejecutando setInterval cada 5s");
	}, 1000);

	/*  Despues de ver setInterval y setTimeout 
        vamos a poner 2 botones para inciar y parar los timers 
    */
	//*Iniciar SetInterval
	// Lo mejor es crear un funcion intervalo()
	function intervalo() {
		var timerInterval = setInterval(() => {
			console.log("Ejecutando setInterval cada 1s");
			var titulo = document.querySelector("#encabezado");
			var bgColor = titulo.style.background;

			if (bgColor == "blue") {
				titulo.style.background = "white";
				titulo.style.color = "black";
			} else {
				titulo.style.background = "blue";
				titulo.style.color = "yellow";
			}

			//alert("Ejecutando setInterval cada 5s");
		}, 1000);

		return timerInterval;
	}

	var timerInterval = intervalo();

	//* Parar setInterval
	var stop = document.querySelector("#stop");
	stop.addEventListener("click", () => {
		alert("Se ha parado el intervalo en bucle de 5s");
		clearInterval(timerInterval);
	});

	//* Inicia setInterval
	var start = document.querySelector("#start");
	start.addEventListener("click", () => {
		alert("Se ha iniciado el intervalo en bucle de 5s");
		intervalo();
	});
});
