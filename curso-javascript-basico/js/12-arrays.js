//* **********     Curso JavaScript: 11. Arreglos (Arrays)      ********** */
"use strict";

const a = [];
const b = [1, true, "Hola", ["A", "B", "C", [1, 2, 3]]];
//const c = Array(); ERROR.

console.log(a);
console.log(b);
console.log(b.length);
console.log(b[2]);
console.log(b[0]);
console.log(b[3]);
console.log(b[3][2]); //B
console.log(b[3][3][0]); //1

//* crea una nueva instancia Array con un número variable de elementos pasados como argumento,
//* independientemente del número o del tipo
const c = Array.of("X", "Y", "Z", 9, 8, 7);
console.log(c);

//* crea un array de 100 posiciones y los rellena con false
const d = Array(100).fill(false);
console.log(d);

const e = new Array();
console.log(e);

const f = new Array(1, 2, 3, true, false);
console.log(f);

const colores = ["Rojo", "Verde", "Azul"];

//* Añadir elemento al array
console.log(colores);
colores.push("Negro");

//* Eliminar ultimo elemento del array
console.log(colores);
colores.pop();

console.log(colores);

//* Recorrer array mediante funcion de callback.
colores.forEach(function (elemento, index) {
	console.log(`<li id="${index}">${elemento}</li>`);
});
console.log("********************************************************");

//* similar pero con arrow function
colores.forEach((elemento, index) => {
	console.log(`<li id="${index}">${elemento}</li>`);
});
