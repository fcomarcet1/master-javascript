"use strict";
/* 
//* Obtener elementos por etiqueta
let todosLosDivs = document.getElementsByTagName("div");
let countDivs = todosLosDivs.length;

console.log(todosLosDivs, countDivs); // Array->HTMLCollection(4) [div#container.container, div, div, div, container: div#container.container] 4

// modificar el 3º div
let ContenidoEnTexto = todosLosDivs[2];
ContenidoEnTexto.style.background = "cyan";

//* Añadir un parrafo a todos los divs en <section id="miseccion"></section>
var seccion = document.querySelector("#miseccion");
var hr = document.createElement("hr");

var valor;
for (valor in todosLosDivs) {
	// valor -> index
	if (typeof todosLosDivs[valor].textContent == "string") {
		//La variable parrafo va tener la funcion de crear los parrafos
		var parrafo = document.createElement("p");

		//La variable texto sirve para colocar el texto de los divs en los parrafos.
		var texto = document.createTextNode(todosLosDivs[valor].textContent);

		//colocar el texto en los párrafos
		parrafo.append(texto);

		//colocar parrafos en seccion
		seccion.append(parrafo);
	}
}
seccion.append(hr);
 */

//* Obtener elementos por su clase css
let divConClassCaja = document.getElementsByClassName("caja");
//console.log(divConClassCaja);

//* Recorrer divs y seleccionar class caja y background azul
for (const index in divConClassCaja) {
	if (divConClassCaja.hasOwnProperty(index)) {
		const element = divConClassCaja[index];
		element.style.background = "blue";
	}
}

//* Recorrer divs y seleccionar class rojo y background red
let divsRojos = document.getElementsByClassName("rojo");
for (let div in divsRojos) {
	if (divsRojos[div].className == "rojo") {
		divsRojos[div].style.background = "red";
	}
}

//* QUERY SELECTOR (Para 1 id o 1 class)
let identif = document.querySelector("#encabezado");
console.log(encabezado);

let id = document.querySelector("#micaja");
console.log(id);

//* QUERY SELECTOR ALL(etiquetas, varios id`s o class)
//* Todos los elemetos div
var matches = document.querySelectorAll("div");

//* devuelve una lista de todos los elementos <div> del documento
//* con una clase "rojo" o "micaja":
var matches = document.querySelectorAll("div.rojo, div.micaja");
