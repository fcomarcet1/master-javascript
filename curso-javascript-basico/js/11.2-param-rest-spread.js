//* *******************PARAMETROS REST Y SPREAD.********************

"use strict";

//* PARAMETROS REST
function listadoFrutas(fruta1, fruta2, ...frutaN) {
	console.log(`Fruta 1: ${fruta1}`);
	console.log(`Fruta 2: ${fruta2}`);

	// El parametro rest se almacena como un array
	console.log(`Fruta N: ${frutaN}`);
	// recorrer array
	frutaN.forEach(function (fruta, index) {
		console.log(fruta);
	});
}

listadoFrutas("Pera", "Melon");
listadoFrutas("Pera", "Melon", "sandia", "ciruela", "membrillo");

//* PARAMETROS SPREAD
const frutas = ["melocoton", "higo"];
listadoFrutas(...frutas, "Pera", "Melon", "sandia", "ciruela", "membrillo");
