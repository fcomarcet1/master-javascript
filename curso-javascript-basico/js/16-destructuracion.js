//* ************************** DESTRUCTURACION ********************
"use strict";

// Usada para asignar valores dinamicamente a arrays y objetos.

const numeros = [1, 2, 3];

// Sin destructuracion
num1 = numeros[0];
num2 = numeros[1];
num3 = numeros[2];

// Con destructuracion
const [num1, num2, num3] = numeros;

console.log(num1, num2, num3);

let persona = {
	nombre: "fran",
	apellidos: "marcet",
	edad: 25,
};

// las variables han de llamarse igual que la propiedad que se desea obtener el valor,
// el orden no importa
let [nombre, apellidos, edad] = persona;

let [apellidos, nombre, edad] = persona;
