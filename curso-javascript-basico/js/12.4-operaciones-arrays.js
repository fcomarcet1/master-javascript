"use strict";

let categorias = ["Accion", "Terror", " Comedia"];
let peliculas = ["ESDLA", "Rambo Acorralado", "Gran torino", true, true, false, "WOW"];

//var elemento = prompt("Inserta un elemento");

//* Añadir elementos en un array hasta que se introduzca "fin"
//* (No ha de introducirse la palabra fin).
do {
	var elemento = prompt("Inserta un elemento");
	peliculas.push(elemento);
} while (elemento != "fin");

const result = peliculas.pop();

console.log(result);

//* Eliminar elemento de un array
//* eliminar ESDLA
let deleteElement = "ESDLA";
// buscar el indice del elemento a eliminar
let index = peliculas.indexOf(deleteElement);

console.log(index);
// comprobamos que existe el indice
if (indice > -1) {
	//eliminamos elementos con splice()
	peliculas.splice(index, 1);
}

//* convertir array a String
// en funcion del separador que pongamos (, -, |), por defecto es ,
let peliculasString = peliculas.join();
console.log(peliculasString);

//* convertir string a array
const texto = ["texto1", "texto2", "texto3"];
let cadena_array = texto.split(", ");
