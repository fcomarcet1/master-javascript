/* **********     Curso JavaScript: 7. Números (Numbers)      ********** */

let a = 2;
let b = new Number(1);
let c = 7.19;
let d = "5.6";

console.log(a, b);
console.log(
  "********* toFixed() -> Convert a number into a string, rounding the number. *****************"
);
console.log(c.toFixed(1)); // 7.2
console.log(c.toFixed(5)); // 7.19000

console.log(
  "********* parseInt() -> Convierte (parsea) un argumento de tipo cadena y devuelve un int. *****************"
);
console.log(parseInt(c)); // 7
console.log(parseInt(d)); // 5

console.log(
  "********* parseFloat() -> Convierte (parsea) un argumento de tipo cadena y devuelve un float .*****************"
);
console.log(parseFloat(c)); // 7.19

console.log("********* typeof*****************");
console.log(typeof c, typeof d); // number string
console.log(a + b);
console.log(c + parseInt(d)); // 12.190000000000001
console.log(c + parseFloat(d)); //12.79

//console.log(c + Number.parseInt(d));
//console.log(c + Number.parseFloat(d));
