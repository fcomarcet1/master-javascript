//* **********     Curso JavaScript: 15. Ciclos (Loops) - #jonmircha     ********** */

//* WHILE
let contador = 0;
while (contador < 10) {
  console.log("while " + contador);
  contador++;
}

//* DO-WHILE
do {
  console.log("do while " + contador);
  contador++;
} while (contador < 10);

//* FOR
/* for (inicialización de variable; condición; decremento o incremento) {
          sentencias que ejecuta el for
          sentencias que ejecuta el for
          sentencias que ejecuta el for
        } */

for (let i = 0; i < 10; i++) {
  console.log("for " + i);
}

// Recorrer array
let numeros = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
for (let i = 0; i < numeros.length; i++) {
  console.log(numeros[i]);
}

// Recorrer objeto
const jon = {
  nombre: "Jon",
  apellido: "MirCha",
  edad: 35,
};

// for in para objetos
for (const propiedad in jon) {
  console.log(`Key: ${propiedad}, Value: ${jon[propiedad]}`);
}

// for of para arrays
for (const elemento of numeros) {
  console.log(elemento);
}

// Recorrer string
let cadena = "Hola Mundo";
for (const caracter of cadena) {
  console.log(caracter);
}
