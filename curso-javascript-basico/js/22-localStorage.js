//* ************************** LOCALSTORAGE *******************
//*Podemos guardar informacion a modo de sesion mientras navegamos en las diferentes paginas
//* Muy usado para aplicaciones completas con js
"use strict";

//* COMPROBAR DISPONIBILIDAD
if (typeof Storage !== "undefined") {
	console.log("LocalStorage compatible");
} else {
	console.log("LocalStorage Incompatible");
}

//*ALMACENAR DATOS (strings, numeros)
localStorage.setItem("titulo", "Curso de Js");

//* RECUPERAR ELEMENTO Y MOSTRARLO EN LA WEB
var storage_titulo = localStorage.getItem("titulo");

//Seleccionamos el div con class="box", y añadimos los datos del storage almacenados en la variable storage_titulo
document.querySelector(".box").innerHTML = storage_titulo;

//* ALMACENAR OBJETO.
//* Para almacenar un objeto en el localStorage, hemos de almacenarlo como un JsonString
const user = {
	nombre: "Fco marcet",
	email: "fcomarcet@mail.com",
	web: "www.fcomarcet.es",
};

//* pasar de Json a JsonString: JSON.stringify(objeto)
localStorage.setItem("user", JSON.stringify(user));

//* RECUPERAR OBJETO Y MOSTRARLO EN LA WEB (Hemos de realizar el paso inverso pasar de JsonString a Json)
//* Pasar de JsonString a Json: JSON.parse(localStorage.getItem("objeto"))
var storage_user = JSON.parse(localStorage.getItem("user"));
console.log(storage_user);

//var box_user = document.querySelector("#box-user").append(user.nombre);

var texto = `<ul>
                <li>${user.nombre}</li>
                <li>${user.email}</li>
                <li>${user.web}</li>
            </ul>`;

var box_user = (document.querySelector("#box-user").innerHTML = texto);

//* ELIMINAR ELEMENTOS LOCALSTORAGE
localStorage.removeItem("user");

//* ELIMINAR TODOS LOS ELEMENTOS DEL LOCALSTORAGE
localStorage.clear();
