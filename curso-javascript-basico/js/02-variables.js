//* UTILIZAR MODO STRICT
'use strict'

//* VARIABLES
let pais = "Españita";
let continente = "Europa";
let age = 25;
let contactos = ['pepe', 'lopez', 636585669];
let verificar = true;


/*
    console.log(pais, continente, verificar);
    console.log(contactos[0], contactos[1], contactos[2] );
*/

//------------------------------------------------------------------------


// *CONCATENACION. 
let pais_y_continente = pais + " " + continente;
let direccion = "Mi pais es: "+ pais +" y pertenece al continente: " + continente ;
console.log(direccion);
console.log(pais_y_continente);
 
//*CASTEAR A TEMPLATE STRING
let direccion2 = `Mi pais es: ${pais} y pertenece al continente: ${continente}` ;
console.log(direccion2);



