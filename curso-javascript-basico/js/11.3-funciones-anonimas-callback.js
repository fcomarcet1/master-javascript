"use strict";

//* ****************** FUNCIONES ANONIMAS ***************************

// funcion que no tiene nombre
let funcionAnonima = function () {
	let texto = "soy una funcion anonima";
	return texto;
};

let result = funcionAnonima();
console.log(result);

// funcion anonima con parametros
let pelicula = function (titulo) {
	return `Hoy se presenta la pelicula: ${titulo}.`;
};

let result2 = pelicula("Fear an loathing in las Vegas");
console.log(result2);

//* FUNCIONES DE CALLBACK
// Es una funcion anonima que recibe como parametro otra funcion fn(fn).

// tenemos n1 , n2 como parametros normales, y SumarYMostrar, sumarPorDos como funciones
//  de callback que son pasadas como parametros a sumame

function sumame(n1, n2, SumarYMostrar, sumarPorDos) {
	let sumar = n1 + n2;
	SumarYMostrar(sumar);
	sumarPorDos(sumar);

	return sumar;
}

sumame(
	4,
	10,
	function (dato) {
		console.log("La suma es: " + dato);
	},
	function (dato) {
		console.log("La suma por 2 es: " + 2 * dato);
	}
);

//* asi cuando se ejecute la funcion principal sumame dentro de
//* esa funcion de ejecuta la funcion de callback
