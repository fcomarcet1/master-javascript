//* **************** PETICIONES AJAX  ********************************
//* Una peticion AJAX es una llamada a un servicio REST (GET, POST, PUT, DELETE),
//*normalmente a un backend o una API externa para obtener los datos en Json.
//*Se utilizara el metodo fetch(),

//* Despues veremos como realizar peticiones AJAX desde JQuery

//* Vamos a usar una Fake API REST -> www.jsonplaceholder.typicode.com
//*                                -> www.reqres.in

"use strict";

//* FETCH PETICIONES A SERVICIOS / API REST MEDIANTE FETCH
var div_users = document.querySelector("#users");
var loading = document.querySelector(".loading");
var usuarios = [];

// El metodo fetch recibe una url
fetch("https://reqres.in/api/users")
	// en la promesa tenemos en metodo then, recogemos los datos con una funcion de callback
	//  que lo convierte en json
	.then((data) => data.json())
	.then((users) => {
		usuarios = users.data;
		console.log(usuarios);

		//recorrer array similar a foreach
		usuarios.map((user, index) => {
			let nombre = document.createElement("h4");
			nombre.innerHTML = `${index} - ${user.first_name} ${user.last_name} `;
			div_users.appendChild(nombre);
			loading.style.display = "none";
		});
	});

// OTRO EJEMPLO CON UN JSON DISTINTO
var users = [];
var div_users2 = document.querySelector("#users-list");
var loading2 = document.querySelector(".loading2");

fetch("https://jsonplaceholder.typicode.com/users")
	// en la promesa tenemos en metodo then, recogemos los datos con una funcion de callback
	//  que lo convierte en json
	.then((data_users) => data_users.json())
	// Almacenamos los datos de data_users convertida a json  en users
	.then((data_users) => {
		users = data_users;
		//console.log(users);

		//recorrer array users
		for (const index in users) {
			console.log(users[index].name);
			let nombre = document.createElement("h4");
			let usuario = users[index];
			nombre.innerHTML = `${index} - ${usuario.name} || ${usuario.email} || ${usuario.address.street} `;
			div_users2.appendChild(nombre);
			loading2.style.display = "none";
		}
	});
