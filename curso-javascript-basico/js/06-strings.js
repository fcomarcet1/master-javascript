/* **********     Curso JavaScript:  Cadenas de Texto (Strings)     ********** */

//*Cadenas de Texto aka Strings
let nombre = "Jon";
let apellido = "MirCha";
let saludo = new String("Hola Mundo");
let lorem =
	"       Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est, et dolorum. Adipisci sequi, voluptates accusamus earum magnam non pariatur odit consequuntur dicta modi. Quam soluta aliquid nam ea quod dolore.      ";

console.log(nombre, apellido, saludo);

console.log(
	nombre.length, //* longitud
	apellido.length,
	saludo.length,
	nombre.toUpperCase(), //* pasar a mayusculas
	apellido.toLowerCase(), //* pasar a minusculas
	lorem.includes("amet"), //* buscar string
	lorem.includes("jon"),
	lorem,
	lorem.trim(), //* elimina espacios al inicio y al final.
	lorem.split(" "),
	lorem.split(",")
);
