"use strict";

const lenguajes = ["PHP", "JS", "Go", "Java", "C", "C++", "Pascal"];

//* buscar en array
let busqueda = lenguajes.find(function (lenguaje) {
	return lenguaje == "PHP"; //PHP
	//return lenguaje == "PHP7"; //undefined
});

console.log(busqueda);

let busqueda2 = lenguajes.find((lenguaje) => {
	return lenguaje == "JS"; //PHP
});
console.log(busqueda2);

// similar al anterior
let busqueda3 = lenguajes.find((lenguaje) => lenguaje == "JS"); //JS
console.log(busqueda3);

//* buscar indice
let busqueda4 = lenguajes.findIndex((lenguaje) => {
	return lenguaje == "JS"; //1
});
console.log(busqueda4);

//*Buscar valores en un array en funcion de ciertas condiciones.
const precios = [25, 12.5, 66, 99];
let busqueda5 = precios.some((precio) => {
	//return precio >= 20; //true
	return precio < 10; //false
});
console.log(busqueda5);
