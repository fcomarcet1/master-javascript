"use strict";

let numeros = [1, 2, 3, 4, 5, 90, 70];
let agenda = ["pepe", "manolo", 636585569, true, "elisa", false, false];

//* Obtener nº de elementos
console.log(numeros.length); //5
console.log(agenda.length); //7

console.log("**********************************");

//* recorrer array
const num_elems = numeros.length;
for (let i = 0; i < num_elems; i++) {
	console.log(numeros[i]);
}

console.log("**********************************");
for (let i = 0; i < agenda.length; i++) {
	console.log(agenda[i]);
}

console.log("**********************************");

document.write("<ul>");
numeros.forEach((elemento, index) => {
	document.write(`<li>[${index}] -> ${elemento}</li>`);
});
document.write("</ul>");
