//* ******************** BOM(Browser Object Model) *****************
//* Elementos del navegador web (tamaño pantalla, redirecciones.. etc)

"use strict";
//* Alto y ancho de la ventana
console.log(window.innerWidth);
console.log(window.innerHeight);
console.log(screen.width);
console.log(screen.height);

//* Obtener URL, protocolo ...etc
//objeto con todos los parametros
console.log(window.location);
console.log(window.location.hostname);
console.log(window.location.href);
console.log(window.location.port);

//*REDIRECCION
function redirect(url) {
	window.location.href = url;
}

//similar
function redireccion(url) {
	location.assign(url);
}

//* ABRIR NUEVA PESTAÑA CON UNA URL
function abrirVentana(url) {
	window.open(url);
	//window.open(url,"","width=400,height=300");
}
