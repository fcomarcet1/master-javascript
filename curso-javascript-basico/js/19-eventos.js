//* *************************** EVENTOS *******************************
//* Son funciones que se ejecutan cada que ocurre alguna accion.
//* Hay diferentes tipos de eventos como con el raton, teclado, foco etc

"use strict";
//*EVENTOS DEL RATON

//* Usando el onclick = "funcion" en el HTML
//let boton = document.querySelector("#boton");

//* LOAD
/* Teniamos el problema que si cargamos el script que interctua con el DOM al principio de 
    / la pagina no funcionaba correctamente ya que no estaban cargadas la etiquetas de la pagina
    / Esto lo podemos arreglar con el evento load()-> window.addEventListener('load', () =>{ codigo});
    */
window.addEventListener("load", () => {
	function cambiaColor() {
		console.log("has dado click");
		var bg = boton.style.backgroundColor;
		console.log(bg);
		if (bg == "green") {
			// si usamos la variable bg asi: bg = "cyan"; no funciona
			boton.style.background = "red"; // asi SI
		} else {
			//bg = "green"; // NO FUNCIONA
			boton.style.background = "green";
		}

		return true;
	}

	var boton = document.querySelector("#boton");

	//* Usando el metodo addEventListener('evento', funcion de callback) cin onclick
	/* boton.addEventListener("click", function () {
        cambiaColor();
    }); */

	boton.addEventListener("click", () => {
		cambiaColor();
		// con el operador this podemos hacer referencia al boton dentro del evento click sin tener
		// que usar boton.style.border  es mas comodo this
		console.log(this);
		this.style.border = "1px dashed purple";
	});

	//* MOUSE OVER
	boton.addEventListener("mouseover", () => {
		boton.style.backgroundColor = "cyan";
		boton.style.width = "100px";
		boton.style.height = "75px";
		boton.style.border = "2px solid yellow";
	});

	//* MOUSEOUT
	boton.addEventListener("mouseout", () => {
		boton.style.backgroundColor = "black";
		boton.style.color = "white";
	});

	//* EVENTOS FOCO Y TECLADO

	var input = document.querySelector("#nombre");

	//*FOCUS
	input.addEventListener("focus", () => {
		console.log("[focus] Estas dentro del campo");
	});

	//* BLUR
	input.addEventListener("blur", function () {
		console.log("[blur] Estas fuera del input");
	});

	//* KEYDOWN
	input.addEventListener("keydown", function (event) {
		console.log("[keydown] Pulsando esta tecla ", String.fromCharCode(event.keyCode));
	});

	//* KEYPRESS
	input.addEventListener("keypress", function (event) {
		console.log("[keypress] Tecla presionada ", String.fromCharCode(event.keyCode));
	});

	//* KEYUP
	input.addEventListener("keyup", function (event) {
		console.log("[keyup] Tecla soltada ", String.fromCharCode(event.keyCode));
	});
}); // End load
