"use strict";

//* DOM(Document Object Model)-> Elementos/etiquetas de la pagina

//*seleccionar elemento por id
let seleccion = document.getElementById("container");
console.log(seleccion);
/*
    <div class=​"container" id=​"container">
    ​   <p>​Hola soy una caja​</p>​
    </div>​
*/

//* Seleccionar el codigo html del elemento seleccionado
let seleccion2 = document.getElementById("container").innerHTML;
console.log(seleccion2); //<p>​Hola soy una caja​</p>

//* Modificar codigo HTML
let caja = document.getElementById("container");
caja.innerHTML = "He cambiado el codigo de la caja";
console.log(caja); //<div class="container" id="container">He cambiado el codigo de la caja</div>

//* Modificar estilos css
caja.style.background = "red";
caja.style.padding = "15px";
caja.style.margin = "15px";

//añadir clase
caja.className = "Cambio-class";

//*--------------------------------------------- */
function cambiarColor(colorBack, colorFont) {
	caja.style.width = "250px";
	caja.style.height = "250px";
	caja.style.margin = "0 auto";
	caja.style.background = colorBack;
	caja.style.color = colorFont;
	caja.className = "Cambio-class class";
}

//* Seleccionar elemento con Query selector. MAS OPTIMA
// Seleccionar por id
let miCaja = document.querySelector("#container");
// Seleccionar por class
let miCaja2 = document.querySelector(".container");
