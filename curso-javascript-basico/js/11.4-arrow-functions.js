"use strict";

//* es una manera de implementar un funcion anonima desde EMC6
let saludar = () => {
	console.log("hola");
};
saludar();

// si la logica de la funcion solo ocupa una linea no es necesario poner las {}
let despedir = () => console.log("Hasta luego lucas");
despedir();

// arrow function con parametros
let saludar2 = (nombre) => {
	console.log("hola", nombre);
};
saludar2("Pepe lotas");

// si solo tenemos un parametro no es necesario ()
let mostrar = (str) => console.log(`Estoy mostrando ${str}`);
mostrar("Motos manolo");

//*NOTA-> Si usamos una arrow function en un objeto es nos cambia el DOM asi que para definir
//* metodos en clases no usar arrow functions
const mascota = {
	nombre: "Kenai",
	edad: 5,
	ladrar() {
		console.log(this);
	},
	correr() {
		console.log("estoy corriendo");
	},
};

console.log(mascota.ladrar());
console.log(mascota.correr());
