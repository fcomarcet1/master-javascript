"use strict";

$(function () {
    let mostrar = $("#mostrar");
    let ocultar = $("#ocultar");
    let div = $("#caja");

    mostrar.hide();

    //* Ocultar div
    ocultar.on("click", function () {
        $(this).hide();
        mostrar.show();
        div.hide("slow", function () {
            console.log("cartel ocultado");
        });
    });

    //* Mostrar div
    mostrar.on("click", function () {
        $(this).hide();
        ocultar.show();
        div.show("slow");
    });

    /* //* Toogle
    let toogle_bottom = $("#bottom-toogle");
    toogle_bottom.on("click", function () {
        div.toggle("slow");
    }); */

    //* fadeToogle, slideToggle
    let toogle_bottom = $("#bottom-toogle");
    toogle_bottom.on("click", function () {
        //div.fadeToggle("slow");
        div.slideToggle("slow");
    });
});
