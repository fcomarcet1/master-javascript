"use strict";

function reloadLinks() {
    //* Seleccionar elementos <a></a> y recorrerlos con .each()
    $("a").each(function (index, element) {
        //console.log($(this));
        let elemento = $(this);
        let enlace = elemento.attr("href");
        // Insertamos el texto en el enlace
        elemento.text(enlace);

        //* AÑADIR ATRIBUTO TARGET
        elemento.attr("target", "_blank");

        //* ELIMINAR ATRIBUTO
        elemento.removeAttr("target", "_blank");
    });
}

// Similar a $(document).ready(function(){});
$(function () {
    console.log("efectos.js cargado");
    //console.log($("a").length);

    reloadLinks();

    //* Capturar valor al pulsar boton (evento click) con id="add-button
    var boton = $("#add-button");
    boton.on("click", function () {
        console.log("he pulsado boton");
        //* Recogemos el valor del input
        //console.log($("#add-link").val());
        $("#menu").prepend(`<li><a href=${$("#add-link").val()}></a></li>`);
        // vacia el input del form
        $("#add-link").val("");
        reloadLinks();
    });
});
