"use strict";

//* PRUEBA FUNCIONAMIENTO jQuery
// 1º Paso comprobar que el documento esta ready

$(document).ready(function () {
    //* Selector de ID
    let rojo = $("#rojo");
    rojo.css("background", "red").css("color", "white");

    let amarillo = $("#amarillo");
    amarillo.css("background", "yellow").css("color", "green");

    let verde = $("#verde");
    verde.css("background", "green").css("color", "white");

    //* Selector de Clase
    let miClase = $(".zebra");

    //* Deprecaded .click(function(){}) USAR on.('evento') NO ME FUNCIONA
    //* NO COGE LA ARROW FUNCTION USAR function()
    $(".sinBorde").click(function () {
        console.log("Click dado");
        $(this).addClass("zebra");
    });

    /* $(".sinBorde").on("click", function () {
		console.log("Click dado");
		$(this).addClass("zebra");
	}); */

    //* Selector de etiqueta
    var parrafo = $("p");
    parrafo.css("cursor", "pointer");

    parrafo.click(function () {
        // para solo acceder al DOM 1 vez
        var thiss = $(this);
        let class_grande = thiss.hasClass("grande");
        //Al pulsar si no tiene la clase grande se la pones y sino la eliminas
        if (!class_grande) {
            thiss.addClass("grande");
        } else {
            thiss.removeClass("grande");
        }
    });

    /* parrafo.on("click", function () {
		$(this).removeClass("zebra");
	}); */

    var div = $(".caja");
    var thiss = $(this);
    let class_div = thiss.has("caja");

    div.on("click", function () {
        console.log("he pulsado en el div");
        if (!class_div) {
            thiss.addClass("grande");
        } else {
            thiss.removeClass("grande");
        }
    });

    //* Selectores de atributo
    var google = $("[title ='Google']");
    var ebay = $("[title ='Ebay']");

    google.css("color", "green");
    ebay.css("color", "green");

    //* Selector de mas de un elemento
    var seleccion = $("p, a");
    console.log(seleccion);
    seleccion.addClass("borde-superior");

    //* Buscar un elemento find()
    // Si no sabemos donde esta
    var busqueda = $(".box").find(".resaltado");
    // Similar
    var buscar = $(".box .resaltado");
    console.log(buscar);

    //* Parent (subir o cambiar a etiquetas padre)
    var busqueda3 = $(".box")
        .eq(0)
        .parent()
        .parent()
        .parent()
        .find("[title ='Ebay']");
    console.log(busqueda3);
});
