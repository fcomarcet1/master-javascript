"use strict";

$(function () {
    //* Load
    /*
    let div_datos = $("#datos");
    div_datos.load("https://reqres.in/"); 
    */

    //* GET -> https://reqres.in/api/users?page=2
    var div_get = $("#datos_get");
    $.get("https://reqres.in/api/users", { page: 2 }, (response) => {
        //console.log(response);
        response.data.forEach((element, index) => {
            //console.log(element.first_name);
            let parrafo = `<p>${element.first_name} - ${element.last_name}</p>`;
            div_get.append(parrafo);
        });
    });

    //* POST
    var formulario = $("#formulario");

    //! NO USAR ARROW FUNCTIONS EN JQUERY
    formulario.on("submit", function (e) {
        // al enviar los datos que no nos redirija a
        //  la url que envia los datos el form
        e.preventDefault();

        //obtener campos del form
        let nombre = $("input[name='nombre']").val(); //let nombre =$("#formulario").name
        let pagina_web = $("input[name='web']").val(); //let pagina_web = $("#formulario").web;

        var usuario = {
            name: nombre,
            web: pagina_web,
        };

        // Obtener url de la action del for
        let url_action_form = $(this).attr("action");

        /* //* peticion POST
        $.post(url_action_form, usuario, function (response) {
            // console.log(response.name);
            let div_post = $("#datos_post");
            let parrafo = `<p>${response.name} - ${response.web}</p>`;
            div_post.append(parrafo);
        }).done(function () {
            alert("Usuario añadido correctamente");
        }); */

        //* $.ajax
        $.ajax({
            type: "POST",
            //dataType: "json",
            //contentType: "aplication/json",
            url: $(this).attr("action"),
            data: usuario,
            beforeSend: function () {
                console.log("enviando usuario");
            },
            success: function (response) {
                console.log(response);
                let div_post = $("#datos_post");
                let parrafo = `<p>${response.name} - ${response.web}</p>`;
                div_post.append(parrafo);
            },
            error: function () {
                console.log(" ha ocurrido un error");
            },
            timeout: 2000, // tiempo max sino error
        });

        return false; // para que no no redirecione al action
    });
});
