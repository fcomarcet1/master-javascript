"use strict";
$(function () {
    console.log("animaciones.js cargado!!");

    //* Animaciones
    let bottom_animation = $("#bottom-animation");
    let caja = $("#caja");
    bottom_animation.on("click", function () {
        caja.animate(
            {
                marginLeft: "500px",
                with: "600px",
                height: "100px",
                fontSize: "40px",
                opacity: "0.5",
            },
            "slow"
        )
            .animate(
                {
                    borderRadius: "900px",
                    marginTop: "80px",
                },
                "slow"
            )
            .animate(
                {
                    borderRadius: "0px",
                    marginLeft: "0px",
                },
                "slow"
            )
            .animate(
                {
                    borderRadius: "100px",
                    marginTop: "0px",
                },
                "slow"
            )
            .animate(
                {
                    marginLeft: "500px",
                    fontSize: "40px",
                    height: "110px",
                },
                "slow"
            );
    });
});
