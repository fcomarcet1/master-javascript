//* #################### EVENTOS EN jQUERY ###############
"use strict";
//! .ready is deprecaded usar .on("evento", function(){};)
/* $(document).ready(function () {
    alert("eventos.js cargado");
}); */

// Similar a lo de arriba
$(function () {
    // Handler for .ready() called.
    //* MOUSEOVER Y MOUSE OUT
    var caja = $("#caja");
    console.log(caja);
    //! .mouseover() is deprecaded usr .on("evento", function)
    /*  caja.mouseover(function () {
        console.log("mouseOver");
        $(this).css("background", "red");
    }); */
    /*  caja.mouseout(function () {
        console.log("mouseOut");
        $(this).css("background", "yellow");
    }); */

    /*  caja.on("mouseover", function () {
        console.log("mouseOver");
        $(this).css("background", "red");
    });

    caja.on("mouseout", function () {
        console.log("mouseOut");
        $(this).css("background", "yellow");
    }); */

    //* HOVER
    function cambiaVerde() {
        $(this).css("background", "green");
    }
    function cambiaAmarillo() {
        $(this).css("background", "yellow");
    }
    //! deprecaded $(selector).hover(inFunction,outFunction)
    //caja.hover(cambiaRojo, cambiaAmarillo);
    caja.on("mouseenter", cambiaVerde).on("mouseleave", cambiaAmarillo);

    //* CLICK Y DBLCLICK
    //! deprecaded .click() / .dblclick()
    /*  caja.click(function () {
        $(this).css("background", "purple").css("color", "white");
    });

    caja.dblclick(function () {
        $(this).css("background", "pink").css("color", "black");
    }); 
    */
    caja.on("click", function () {
        $(this).css("background", "purple").css("color", "white");
    });
    caja.on("dblclick", function () {
        $(this).css("background", "pink").css("color", "black");
    });

    //* FOCUS Y BLUR
    // seleccionamos el input con id="nombre"
    var inputName = $("#nombre");
    inputName.on("focus", function () {
        $(this).css("border", "solid 2px pink");
    });
    inputName.on("blur", function () {
        $(this).css("border", "solid 2px blue");
        // Obtenemos el valor introducido en el input .val()
        let datos = $(this).val();
        console.log(datos);
        //mostrar atos en una caja
        $("#datos").text(datos).show();
    });

    //* MOUSEDOWN ,MOUSEUP, MOUSEMOVE
    var datos = $("#datos");
    // Mousedown
    datos.on("mousedown", function () {
        console.log("pulsado mouseDown");
        $(this).css("border-color", "red");
    });
    // Mouseup
    datos.on("mouseup", function () {
        console.log("pulsado mouseUp");
        $(this).css("border-color", "green");
    });
    // Mousemove
    $(document).on("mousemove", function () {
        let coord_x = event.clientX;
        let coord_y = event.clientY;
        console.log(`Coord X: ${coord_x}`);
        console.log(`Coord Y: ${coord_y}`);

        var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
        console.log(pageCoords);

        //* Mover un div con mousemove(Puntero raton)
        $("body").css("cursor", "none");
        let div_sigueme = $("#sigueme");
        div_sigueme.css("left", coord_x); //  Eje X
        div_sigueme.css("top", coord_y); // Eje Y
    });
});
